/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Victor Sabariego Cossin]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, OPTIONS, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 800;
    const int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    
    Color logoColor = BLACK;          
	logoColor.a = 0;
    
    Color GoColor = RED;
    GoColor.a = 255;
    
    Color GoNumColor = RED;
    GoNumColor.a = 255;
    
    Color BackPauseColor = RED;
    BackPauseColor.a = 50;
    
    int BallSpeed = 4;
    int SumBallSpeed = 1;
    int MaxBallSpeed = 10;
    
    int iaLine = screenWidth/2;
    
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;    
    player.width = 15;
    player.height = 63;    
    player.x = 60 - player.width;
    player.y = screenHeight/2 - player.height/2;
        
    int playerSpeedY = 8;
    
    Rectangle enemy;    
    enemy.width = 15;
    enemy.height = 63;    
    enemy.x = screenWidth - 50 - enemy.width;
    enemy.y = screenHeight/2 - enemy.height/2;
    
    int enemySpeedY = 8;
    
    Vector2 ballPosition;    
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
    ballSpeed.x = BallSpeed; 
    ballSpeed.y = BallSpeed;
    
    int ballRadius = 10;
    
    int playerLife = 100;
    
    Rectangle PlayerLife;
    PlayerLife.width = 300;
    PlayerLife.height = 40;    
    PlayerLife.x = 30;
    PlayerLife.y = 10;
   
    int enemyLife = 100;
    
    Rectangle EnemyLife;
    EnemyLife.width = 300;
    EnemyLife.height = 40;    
    EnemyLife.x = screenWidth/2 + 67;
    EnemyLife.y = 10;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    int wins = 0;           // Number of winnings    
    
    Vector2 TitlePosition;          // Position of the title of the game
    
    TitlePosition.x = screenWidth/2 - 190;
    TitlePosition.y = screenHeight/2 - 400;
    
        
    Vector2 TitlePositionR;           // Position of the title of the game in red
    
    TitlePositionR.x = screenWidth/2 - 192;
    TitlePositionR.y = screenHeight/2 - 400;

    Vector2 PlayTitlePos;            // Position of the play option
    
    PlayTitlePos.x = screenWidth/2 - 55;
    PlayTitlePos.y = screenHeight/2;

    Vector2 RecSizeTitle;          // Size of the rectangle of the Title screen
     
    RecSizeTitle.x = 200;
    RecSizeTitle.y = 1;
    
    Vector2 RecPosTitle;           // Position of the rectangle for the Title screen

    RecPosTitle.x = 0;
    RecPosTitle.y = screenHeight/2;
    
    Vector2 OpTitlePos;          // Position of the options of the game
    
    OpTitlePos.x = screenWidth/2 - 100;
    OpTitlePos.y = screenHeight/2 + 60;
    
    Vector2 ReiniciarPos;            // Position of the option "REINICIAR" of pause state
    
    ReiniciarPos.x = screenWidth/2 - 150;
    ReiniciarPos.y = screenHeight/2 - 70;
    
    Vector2 ContinuarPos;          // Position of the option "CONTINUAR" of pause state
    
    ContinuarPos.x = screenWidth/2 - 150;
    ContinuarPos.y = screenHeight/2;

    Vector2 SalirPausePos;          // Position of the option "SALIR" of pause state
    
    SalirPausePos.x = screenWidth/2 - 150;
    SalirPausePos.y = screenHeight/2 + 70;
    
    Vector2 PausePos;
    
    PausePos.x = screenWidth/2 - 100;
    PausePos.y = screenHeight/2 - 200;
    
    Vector2 PausePosB;
    
    PausePosB.x = screenWidth/2 - 96;
    PausePosB.y = screenHeight/2 - 198;
    
    Vector2 DificultadPos;
    
    DificultadPos.x = screenWidth/2 - 180;
    DificultadPos.y = screenHeight/2;    
    
    Vector2 DificultadPos2;
    
    DificultadPos2.x = screenWidth/2 + 70;
    DificultadPos2.y = screenHeight/2;
    
    int RadiusCircle1 = 0;         // Radius of the background of the title screen
    int RadiusCircle2 = 0;
    int RadiusCircle3 = 0;
    
    int SelectDif = 0;
    
    int TimerIniGame = 3;
    
    // Podium ending screen
    Rectangle Podio1;
    Podio1.width = 150;
    Podio1.height = 250;    
    Podio1.x = 180;
    Podio1.y = screenHeight;

    Rectangle Podio2;
    Podio2.width = 150;
    Podio2.height = 350;    
    Podio2.x = Podio1.x + Podio1.x - 25;
    Podio2.y = screenHeight;
    
    Rectangle Podio3;
    Podio3.width = 150;
    Podio3.height = 350;    
    Podio3.x = Podio2.x + 155;
    Podio3.y = screenHeight;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    Font font = LoadFontEx ( "resources/cubic.ttf", 93, 0, 250 );
    Font DefFont = GetFontDefault();
    
    Texture2D BackgroundGameplay = LoadTexture("resources/fondo_gameplay.png");
    
    InitAudioDevice();
    
    Sound fxWav1 = LoadSound("resources/CambioDeSeleccion.wav");
    Sound fxWav2 = LoadSound("resources/SelectSound.wav");
    Sound fxWav3 = LoadSound("resources/logo.wav");
    Sound fxWav4 = LoadSound("resources/CuentaAtras.wav");
    Sound fxWav5 = LoadSound("resources/Go.wav");
    Sound fxWav6 = LoadSound("resources/WIN.wav");
    Sound fxWav7 = LoadSound("resources/hitBall.wav");
    Sound fxWav8 = LoadSound("resources/Goal.wav");
    
    Music music = LoadMusicStream("resources/Royalty Free Game Music - 8 Bit Space Groove by HeatleyBros.ogg");
    Music end = LoadMusicStream("resources/ENDING.ogg");
    
    PlayMusicStream(music);
    
    SetTargetFPS(60);
    
    bool blink = false;
    bool SoundLogo = false;
    bool pause = true;
    bool fin = false;
    bool win = false;
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
				if (SoundLogo == false)
                {
                    PlaySound(fxWav3);
                    SoundLogo = true;
                }
                
                if (framesCounter < 90){
                    if(logoColor.a + 3 < 255){
                        logoColor.a+= 3;
                    }else{
                        logoColor.a = 255;
                    }
                }    
                else if(framesCounter > 210){
                    if(logoColor.a - 3 > 0){
                        logoColor.a-= 3;
                    }else{
                        logoColor.a = 0;
                    }
                }
                
                if (framesCounter > 300) 
				{
					screen = TITLE;
                    logoColor.a = 255;
					framesCounter = 0;
				}
                
                if (IsKeyDown(KEY_SPACE))
                {
                    screen = TITLE;
                }
                
            } break;
            case TITLE: 
            {
                UpdateMusicStream(music);
                PlayMusicStream(music);
                
                // Background animation
                RadiusCircle1 += 25;
                RadiusCircle2 += 25;
                RadiusCircle3 += 25;
                
                if(RadiusCircle1 >= 300)
                {
                    RadiusCircle1 = 300;
                }                
                if(RadiusCircle2 >= 500)
                {
                    RadiusCircle2 = 500;
                }                
                if(RadiusCircle3 >= 700)
                {
                    RadiusCircle3 = 700;
                }    
                
                // Animation of the controler of the menu
                if(RecSizeTitle.x >= 200)
                {
                    RecSizeTitle.x += 17;
                }
                if(RecSizeTitle.x >= 440)
                {
                    RecSizeTitle.x = 440;
                }
                
                RecSizeTitle.y += 4;
                
                if(RecSizeTitle.y >= 30)
                {
                    RecSizeTitle.y = 30;
                }
                // Animation of the title
                if(TitlePosition.y < screenHeight/2 - 165)
                {
                    TitlePosition.y += 10;
                }
                else
                {
                    TitlePosition.y = screenHeight/2 - 165;
                }
                if(TitlePositionR.y < screenHeight/2 - 162)
                {
                    TitlePositionR.y += 10;
                }
                else
                {
                    TitlePositionR.y = screenHeight/2 - 162;
                } 
                // Controler of the menu
                if(IsKeyPressed(KEY_DOWN))
                {
                    PlaySound(fxWav1);
                    
                    RecSizeTitle.x -= 17;
                    RecSizeTitle.y -= 4;
                    RecPosTitle.y = screenHeight/2 + 60;
                    if(RecSizeTitle.x <= 200)
                    {
                        RecSizeTitle.x = 200;
                    }
                    if(RecSizeTitle.x = 200)
                    {
                        RecSizeTitle.x +=17;
                    }
                    if(RecSizeTitle.y <= 0)
                    {
                        RecSizeTitle.y += 4;
                    }
                    if(RecPosTitle.y >= screenHeight/2 + 60)
                    {
                        RecPosTitle.y = screenHeight/2 + 60;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    PlaySound(fxWav1);
                    
                    RecSizeTitle.x = 200;
                    
                    if(RecPosTitle.y <= screenHeight/2 + 60)
                    {
                        RecPosTitle.y = screenHeight/2;
                    }
                     if(RecSizeTitle.x >= 200)
                    {
                        RecSizeTitle.x += 17;
                    }
                    if(RecSizeTitle.x >= 440)
                    {
                        RecSizeTitle.x = 440;
                    }
                
                    RecSizeTitle.y += 4;
                
                    if(RecSizeTitle.y >= 30)
                    {
                        RecSizeTitle.y = 30;
                    }   
                }
                
                if(RecPosTitle.y == screenHeight/2)
                {
                    if (IsKeyDown(KEY_ENTER))
                    {
                        PlaySound(fxWav2);
                        
                        StopMusicStream(music);
                        
                        screen = GAMEPLAY;
                    }
                }                
                if(RecPosTitle.y == screenHeight/2 + 60)
                {
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        PlaySound(fxWav2);
                        
                        screen = OPTIONS;
                        RadiusCircle1 = 0;
                        RadiusCircle2 = 0;
                        RadiusCircle3 = 0;
                    }
                }
                framesCounter++;
                if (framesCounter % 15 == 0)
                {
                    framesCounter = 0;
                    blink = !blink;
                }
            } break;
            case OPTIONS:
            {                
                UpdateMusicStream(music);
                
                if(IsKeyPressed(KEY_RIGHT))
                {
                    PlaySound(fxWav1);
                    
                    SelectDif ++;
                }
                if(IsKeyPressed(KEY_LEFT))
                {
                    PlaySound(fxWav1);
                    
                    SelectDif --;
                }
                
                if(SelectDif >= 2)
                {
                    SelectDif = 2;
                }
                if(SelectDif <= 0)
                {
                    SelectDif = 0;
                }
                RadiusCircle1 += 25;
                RadiusCircle2 += 25;
                RadiusCircle3 += 25;
                
                if(RadiusCircle1 >= 300)
                {
                    RadiusCircle1 = 300;
                }                
                if(RadiusCircle2 >= 500)
                {
                    RadiusCircle2 = 500;
                }                
                if(RadiusCircle3 >= 700)
                {
                    RadiusCircle3 = 700;
                }    
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    PlaySound(fxWav2);
                    
                    screen = TITLE;
                }
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                UpdateMusicStream(music);
                PlayMusicStream(music);
                
                framesCounter ++;
                if(framesCounter == 1)
                {
                    PlaySound(fxWav4);
                }
                if(framesCounter == 60)
                {
                    if(TimerIniGame >= 3)
                    {
                        PlaySound(fxWav4);
                        TimerIniGame -= 1;
                    }
                }
                if(framesCounter == 120)
                {
                    if(TimerIniGame >= 2)               
                    {
                        PlaySound(fxWav4);
                        TimerIniGame -= 1;
                    }
                }
                if(framesCounter == 180)
                {
                    PlaySound(fxWav5);
                    TimerIniGame = -1;
                    GoColor.a -= 3;
                    GoNumColor.a = 0;
                }
                if(framesCounter >= 240)
                {
                    GoColor.a = 0;
                }
                if(TimerIniGame > -1)
                {
                    pause = true;
                }
                if(TimerIniGame == -1)
                {
                    pause = false;
                }
                if(TimerIniGame == -1)
                {
                    TimerIniGame = -2;
                }
                
                // TODO: Ball movement logic.........................(0.2p)    
                
                if(pause == false && fin == false)
                {
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y; 
                }
                
                if( ballPosition.y >= screenHeight - ballRadius || ballPosition.y <= 0 + ballRadius ){
                    ballSpeed.y = -ballSpeed.y;
                    PlaySound(fxWav7);
                }
                // TODO: Player movement logic.......................(0.2p)                
                if(pause == false && fin == false)
                {
                    if (IsKeyDown(KEY_UP))
                    {
                        player.y -= playerSpeedY;
                    }
                    
                    if (player.y > screenHeight - player.height)
                    {
                        player.y = screenHeight - player.height;
                    }
                    
                    if (player.y < 0)
                    {
                        player.y = 0;
                        
                    }
                    
                    if (IsKeyDown(KEY_DOWN))
                    {
                        player.y += playerSpeedY;
                    }
                    
                }
                // TODO: Enemy movement logic (IA)...................(1p)
                if(pause == false && fin == false)
                {

                    if (ballPosition.x > iaLine){
                        if(ballPosition.y > enemy.y){
                            enemy.y += enemySpeedY;
                        }
                        if(ballPosition.y < enemy.y){
                            enemy.y -= enemySpeedY;
                        }
                    }
                    if(enemy.y > screenHeight - enemy.height){
                        enemy.y = screenHeight - enemy.height;
                    }
                    if(enemy.y == 0 + enemy.height){
                        enemy.y = 0 + enemy.height;
                    }
                }                
                // Dificulty system
                if (SelectDif == 0)  
                {
                    iaLine = screenWidth/2 + 100;
                    enemySpeedY = 5;
                }
                if (SelectDif == 1)
                {
                    iaLine = screenWidth/2 - 100;
                    SumBallSpeed = 2.2;
                }
                if (SelectDif == 2)
                {
                    enemySpeedY = 9;
                    iaLine = screenWidth/2 - 200;
                    SumBallSpeed = 3.6;
                }

                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if(CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                    PlaySound(fxWav7);
                    if(ballSpeed.x<0){
                        if(abs(ballSpeed.x)<BallSpeed){
                            ballSpeed.x = -ballSpeed.x;
                            ballSpeed.x += SumBallSpeed;
                            ballSpeed.y += SumBallSpeed;
                        }
                    }
                }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                    PlaySound(fxWav7);
                    if(ballSpeed.x>0){
                        if(abs(ballSpeed.x)<BallSpeed){
                            ballSpeed.x = -ballSpeed.x;
                            ballSpeed.x -= SumBallSpeed;
                            ballSpeed.y -= SumBallSpeed;
                        }
                    }
                }
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if (ballPosition.x >= screenWidth - ballRadius){
                    PlaySound(fxWav8);
                    ballPosition.x = screenWidth/2;
                    ballSpeed.x = -4;
                    ballSpeed.y = -4;
                    enemyLife -= 33;
                    EnemyLife.width -= 100;
                }
                else if (ballPosition.x <= 0 + ballRadius)
                {
                    PlaySound(fxWav8);
                    ballPosition.x = screenWidth/2;
                    ballSpeed.x = 4;
                    ballSpeed.y = 4;
                    playerLife -= 33;
                    PlayerLife.width -= 100;
                }                
                if(ballSpeed.x >= MaxBallSpeed && ballSpeed.y >= MaxBallSpeed)
                {
                    ballSpeed.x = MaxBallSpeed;
                    ballSpeed.y = MaxBallSpeed;
                }
                
                // TODO: Life bars decrease logic....................(1p) // Esta escrito en la linea 580, porque aquí no me funciona.
                /*if(ballPosition.x > screenWidth - ballRadius)
                {
                    enemyLife -= 33;
                    EnemyLife.width -= 100;
                }else if(ballPosition.x <= 0 + ballRadius)
                {
                    playerLife -= 33;
                    PlayerLife.width -= 100;
                }*/

                // TODO: Time counter logic..........................(0.2p)
                if(pause == false && fin == false)
                {
                    if(framesCounter > 240 && secondsCounter > 0)
                    {         
                        if(framesCounter >= 300)
                        {
                            secondsCounter--;
                            framesCounter = 239;
                            framesCounter++;
                        }
                    }
                }

                // TODO: Game ending logic...........................(0.2p)
                if(EnemyLife.width == 0)
                {
                    TimerIniGame--;
                    gameResult = 1;
                    fin = true;
                    StopMusicStream(music);
                    win = true;
                }
                if(PlayerLife.width == 0)
                {
                    TimerIniGame--;
                    gameResult = 0;
                    fin = true;
                    StopMusicStream(music);
                }
                if(secondsCounter <= 0)
                {
                    TimerIniGame--;
                    framesCounter = 301;
                    fin = true;
                    StopMusicStream(music);
                    if(enemyLife < playerLife)
                    {
                        gameResult = 1;
                        win = true;
                    }
                }
                if(TimerIniGame == -3)
                {
                    PlaySound(fxWav6);
                    if(win == true && gameResult == 1)
                    {
                        wins += 1;
                    }
                }
                if(fin == true && TimerIniGame == -172)
                {
                    screen = ENDING;
                    framesCounter = 0;
                    BallSpeed = 4;
                }
                
                // TODO: Pause button logic..........................(0.2p)
                if(IsKeyPressed(KEY_P))
                {
                    pause = !pause;

                }
                if(TimerIniGame == -2 && pause == true)
                {
                    PauseMusicStream(music);
                    
                    if(IsKeyPressed(KEY_S))
                    {   
                        StopMusicStream(music);    
                        screen = LOGO;
                        pause = !pause;
                        framesCounter = 0;
                        TimerIniGame = 3;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        player.y = screenHeight/2 - player.height/2;
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        GoColor.a = 255;
                        GoNumColor.a = 255;
                        RadiusCircle1 = 0;         // Radius of the background of the title screen
                        RadiusCircle2 = 0;
                        RadiusCircle3 = 0;
                        TitlePosition.x = screenWidth/2 - 190;
                        TitlePosition.y = screenHeight/2 - 400;
                        TitlePositionR.x = screenWidth/2 - 192;
                        TitlePositionR.y = screenHeight/2 - 400;
                        RecSizeTitle.x = 200;
                        RecSizeTitle.y = 1;
                        RecSizeTitle.x = 200;
                        RecSizeTitle.y = 1;
                        playerLife = 100;
                        PlayerLife.width = 300;
                        enemyLife = 100;
                        EnemyLife.width = 300;
                        gameResult = -1;
                        BallSpeed = 4;
                        enemySpeedY = 8;
                    }
                    if(IsKeyPressed(KEY_R))
                    {
                        StopMusicStream(music);
                        pause = !pause;
                        framesCounter = 0;
                        TimerIniGame = 3;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        player.y = screenHeight/2 - player.height/2;
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        GoColor.a = 255;
                        GoNumColor.a = 255;
                        secondsCounter = 99;
                        playerLife = 100;
                        PlayerLife.width = 300;
                        enemyLife = 100;
                        EnemyLife.width = 300;
                        gameResult = -1;
                        BallSpeed = 4;
                    }

                }
            } break;
            case ENDING: 
            {
                framesCounter++;
                PlayMusicStream(end);
                UpdateMusicStream(end);
                // Update END screen data here!
                Podio1.y -= 3;
                if(framesCounter >= 50)
                {
                    Podio1.y = screenHeight/2 + 50;
                }
                if(framesCounter >= 20)
                {
                    Podio2.y -= 3;
                }
                if(framesCounter >= 100)
                {
                    Podio2.y = screenHeight/2 - 30;
                    framesCounter = 100;
                }
                if(framesCounter >= 40)
                {
                    Podio3.y -= 3;
                }
                if(framesCounter >= 90)
                {
                    Podio3.y = screenHeight/2 + 20;
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                if(framesCounter >= 100 && IsKeyPressed(KEY_R))
                {
                    StopMusicStream(end);
                    screen = TITLE;
                    StopMusicStream(music);
                    pause = !pause;
                    framesCounter = 0;
                    TimerIniGame = 3;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    player.y = screenHeight/2 - player.height/2;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    GoColor.a = 255;
                    GoNumColor.a = 255;
                    secondsCounter = 99;
                    playerLife = 100;
                    PlayerLife.width = 300;
                    enemyLife = 100;
                    EnemyLife.width = 300;
                    fin = false;
                    Podio1.y = screenHeight;                    
                    Podio2.y = screenHeight;                    
                    Podio3.y = screenHeight;
                    gameResult = -1;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawText("Victor Sabariego", 53, 170, 80, logoColor);
                    DrawText("Victor Sabariego", 55, 173, 80, RAYWHITE);
                    DrawText("presenta", 334, 235, 30, logoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: DrawTitle..............................(0.2p)
                    DrawCircle(screenWidth/2, screenHeight/2, RadiusCircle3, BLACK);
                    DrawCircle(screenWidth - 250, -10, RadiusCircle2, RAYWHITE);
                    DrawCircle(700, -10, RadiusCircle1, WHITE);
                    DrawTextEx( font, "FINAL PONG" , TitlePositionR, 59, 5, RED); 
                    DrawTextEx( font, "FINAL PONG" , TitlePosition, 59, 5, BLACK);
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    DrawRectangle(RecPosTitle.x, RecPosTitle.y,RecSizeTitle.x, RecSizeTitle.y, RED);
                    if(blink) DrawTextEx( font, "JUGAR" , PlayTitlePos, 30, 5, BLACK);
                    if(!blink) DrawTextEx( font, "JUGAR" , PlayTitlePos, 30, 5, RAYWHITE);
                    DrawTextEx( font, "OPCIONES" , OpTitlePos, 30, 5, BLACK);
                    DrawText("[esc] salir", 10, screenHeight - 30, 20, RED);
                    DrawText("[ENTER] Aceptar", screenWidth - 180, screenHeight - 30, 20, RED); 
                    
                } break;
                case OPTIONS:
                {
                    DrawCircle(screenWidth/2, screenHeight/2, RadiusCircle3, BLACK);
                    DrawCircle(screenWidth - 250, -10, RadiusCircle2, RAYWHITE);
                    DrawCircle(700, -10, RadiusCircle1, WHITE);
                    DrawTextEx(font, "OPCIONES" , TitlePosition, 59, 5, RED);
                    DrawText("DIFICULTAD", DificultadPos.x, DificultadPos.y, 30, BLACK);
                    DrawText("[ENTER] aplicar", 10, screenHeight - 30, 20, RED);
                    
                    if(SelectDif == 0 )
                    {
                        DrawText("< fácil >", DificultadPos2.x, DificultadPos2.y, 30, BLACK);
                    }
                    if(SelectDif == 1)
                    {
                        DrawText("< casi difícil >", DificultadPos2.x, DificultadPos2.y, 30, BLACK);
                    }
                    if(SelectDif == 2)
                    {
                        DrawText("< difícil >", DificultadPos2.x, DificultadPos2.y, 30, BLACK);
                    }
                } break;
                case GAMEPLAY:
                { 
                    DrawTexture( BackgroundGameplay, 0, 0, RAYWHITE);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangle(30 - 3, 10 - 3, 300 + 6, 40 + 6, BLACK);
                    DrawRectangleRec(PlayerLife, RED);
                    DrawRectangle(screenWidth/2 + 64, 10 - 3, 300 + 6, 40 + 6, BLACK);
                    DrawRectangleRec(EnemyLife, RED);
                    DrawText(FormatText ("%d", enemyLife), screenWidth/2 + 70, 8, 50, BLACK);
                    DrawText(FormatText ("%d", playerLife), 34, 8, 50, BLACK);
                    
                    // Draw GAMEPLAY screen here!
                    DrawCircleV(ballPosition, ballRadius, RAYWHITE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, BLUE);
                    
                    DrawRectangleRec(enemy, RED); 

                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText ("%d", secondsCounter), screenWidth/2 - 28, 10, 50, RED);
                    DrawText(FormatText ("%d", secondsCounter), screenWidth/2 - 30, 8, 50, BLACK);
                     
                    // TODO: Draw pause message when required........(0.5p)
                    DrawText("[ flecha arriba ] pala azul arriba", 27, screenHeight/2 - 80, 20, GoNumColor);
                    DrawText("[ flecha abajo ] pala azul abajo", 27, screenHeight/2 + 50, 20, GoNumColor);
                    DrawText("[ P ] PAUSA", 100, screenHeight/2 + 10, 20, GoNumColor);
                    DrawText(FormatText ("%d", TimerIniGame), screenWidth/2 - 10, screenHeight/2 - 30, 50, GoNumColor);
                    if(framesCounter >= 180)
                    {
                        DrawText("[ flecha arriba ] pala azul arriba", 27, screenHeight/2 - 80, 20, GoColor);
                        DrawText("[ flecha abajo ] pala azul abajo", 27, screenHeight/2 + 50, 20, GoColor);
                        DrawText("GO!", screenWidth/2 - 50, screenHeight/2 - 50, 80, GoColor);
                        DrawText("[ P ] PAUSA", 100, screenHeight/2 + 10, 20, GoColor);
                    }
                    
                    if(pause == true && TimerIniGame == -2)
                    {
                        DrawRectangle(0, 0, screenWidth, screenHeight, BackPauseColor);
                        DrawTextEx(font, "PAUSE", PausePosB, 50, 2, BLACK);
                        DrawTextEx(font, "PAUSE", PausePos, 50, 3, RED);
                        DrawTextEx(DefFont, "[R] REINICIAR", ReiniciarPos, 30, 5, BLACK);
                        DrawTextEx(DefFont, "[S] SALIR" , SalirPausePos, 30, 5, BLACK);
                        DrawTextEx(DefFont, "[P] CONTINUAR" , ContinuarPos, 30, 5, BLACK);
                    }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLACK);
                    DrawRectangleRec(Podio1, BROWN);
                    DrawRectangleRec(Podio2, GOLD);
                    DrawRectangleRec(Podio3, LIGHTGRAY);
                    DrawText("[R] REINTENTAR", 10, screenHeight - 27, 17, WHITE);
                    DrawText("[ESC] SALIR", screenWidth - 110, screenHeight - 27, 17, WHITE);
                    if(framesCounter >= 100)
                    {
                        DrawText(FormatText ("%d", wins), screenWidth/2 - 5, screenHeight/2, 50, BLACK);
                        DrawText(FormatText ("%d", wins), screenWidth/2 - 2, screenHeight/2 - 2, 50, GOLD);
                    }
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if(gameResult == 1)
                    {
                        DrawText("¡GANÁSTE!", screenWidth/2 - 120, screenHeight/2 - 100, 50, GOLD);
                    }
                    if(gameResult == 0)
                    {
                        DrawText("perdiste", screenWidth/2 - 90, screenHeight/2 - 100, 50, RED);
                    }
                    if(secondsCounter <= 0)
                    {
                        DrawText("El timepo se acabó amigo", 250, 10, 27, WHITE);
                        if(enemyLife < playerLife)
                        {
                            DrawText("¡GANÁSTE!", screenWidth/2 - 120, screenHeight/2 - 100, 50, GOLD);
                        }
                        if(playerLife < enemyLife)
                        {
                            DrawText("perdiste", screenWidth/2 - 90, screenHeight/2 - 100, 50, RED);
                        }
                        if(enemyLife == playerLife || playerLife == enemyLife)
                        {
                            DrawText("EMPATE", screenWidth/2 - 90, screenHeight/2 - 100, 50, GRAY);
                        }
                    }
                    
                    
                } break;
                default: break;
            }
        
            DrawFPS(screenWidth - 80, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadMusicStream(music);
    UnloadMusicStream(end);
    
    UnloadSound(fxWav1);
    UnloadSound(fxWav2);
    UnloadSound(fxWav3);
    UnloadSound(fxWav4);
    UnloadSound(fxWav5);
    UnloadSound(fxWav6);
    
    CloseAudioDevice(); 
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}